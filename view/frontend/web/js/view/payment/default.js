/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Afterpay_Payment/js/view/model/conditions-validator'
    ],
    function (Component, rendererList, additionalValidators, conditionsValidator) {
        'use strict';

        rendererList.push(
            {
                type: 'afterpay_nl_digital_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_nl_digital_invoice'
            },
            {
                type: 'afterpay_nl_digital_invoice_extra',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_nl_digital_invoice_extra'
            },
            {
                type: 'afterpay_nl_direct_debit',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_nl_direct_debit'
            },
            {
                type: 'afterpay_nl_business_2_business',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_nl_business_2_business'
            },
            {
                type: 'afterpay_be_digital_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_be_digital_invoice'
            },
            {
                type: 'afterpay_be_digital_invoice_extra',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_be_digital_invoice_extra'
            },
            {
                type: 'afterpay_de_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_de_digital_invoice'
            },
            {
                type: 'afterpay_de_direct_debit',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_de_direct_debit'
            },
            {
              type: 'afterpay_de_invoice_extra',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_de_digital_invoice_extra'
            },
            {
              type: 'afterpay_de_installment',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_de_installment'
            },
            {
              type: 'afterpay_at_open_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_at_open_invoice'
            },
            {
              type: 'afterpay_at_direct_debit',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_at_direct_debit'
            },
            {
              type: 'afterpay_at_installment',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_at_installment'
            },
            {
                type: 'afterpay_ch_open_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_ch_open_invoice'
            },
            {
                type: 'afterpay_se_open_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_se_open_invoice'
            },
            {
                type: 'afterpay_se_installment',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_se_installment'
            },
            {
                type: 'afterpay_fi_open_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_fi_open_invoice'
            },
            {
                type: 'afterpay_fi_installment',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_fi_installment'
            },
            {
                type: 'afterpay_no_open_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_no_open_invoice'
            },
            {
                type: 'afterpay_no_installment',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_no_installment'
            },
            {
                type: 'afterpay_no_flex',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_no_flex'
            },
            {
                type: 'afterpay_dk_digital_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_dk_digital_invoice'
            }
        );
        additionalValidators.registerValidator(conditionsValidator);
        return Component.extend({});
    }
);