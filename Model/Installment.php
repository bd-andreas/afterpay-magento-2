<?php
/**
 * Copyright (c) 2018  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2018 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model;

use Afterpay\Payment\Api\InstallmentInterface;
use Afterpay\Afterpay;
use Afterpay\Payment\Helper\Service\Data;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Checkout\Model\Session as CheckoutSession;

/**
 * Class Installment
 *
 * @package Afterpay\Payment\Model
 */
class Installment implements InstallmentInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var Afterpay
     */
    protected $afterpay;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var array
     */
    protected $allowedMethods = [
        Data::AFTERPAY_DE_IN,
        Data::AFTERPAY_FI_IN,
        Data::AFTERPAY_AT_IN,
        Data::AFTERPAY_NO_IN,
        Data::AFTERPAY_SE_IN
    ];

    /**
     * @var CheckoutSession
     */
    protected $session;

    /**
     * @var Magento\Directory\Model\Currency
     */
    protected $currency;

    /**
     * Ajax constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param Afterpay $afterpay
     * @param CheckoutSession $session
     * @param Currency $currency
     * @param Data $helper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Afterpay $afterpay,
        CheckoutSession $session,
        Currency $currency,
        Data $helper
    )
    {
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->afterpay = $afterpay;
        $this->session = $session;
        $this->currency = $currency;
    }

    /**
     * @param string $paymentMethod
     *
     * @return array|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function lookup($paymentMethod)
    {
        if (\in_array($paymentMethod, $this->allowedMethods, true)) {
            $auth = $this->getConfiguration($paymentMethod);
            $requestData = [
                'amount' => round($this->session->getQuote()->getGrandTotal(), 2)
            ];
            $this->afterpay->setRest();
            $this->afterpay->set_ordermanagement('installmentplans_lookup');
            $this->afterpay->set_order($requestData, 'OM');
            $this->afterpay->do_request($auth, $auth['mode']);
            return $this->parseResponse($this->afterpay->order_result->return);
        }
    }

    /**
     * @param $path
     * @param $paymentMethod
     *
     * @return mixed
     */
    private function loadConfig($path, $paymentMethod)
    {
        $path = 'payment/' . $paymentMethod . '/' . $path;
        return $this->scopeConfig->getValue($path);
    }

    /**
     * @param $paymentMethod string
     *
     * @return mixed
     */
    private function getConfiguration($paymentMethod)
    {
        $connectionType = $this->loadConfig('testmode', $paymentMethod);
        $result['modus'] = $this->helper->getConnectionType($connectionType);
        $result['mode'] = $this->helper->getConnectionType($connectionType, true);
        $result['apiKey'] = $this->loadConfig($result['modus'] . '_api_key', $paymentMethod);
        return $result;
    }

    /**
     * @param \stdClass $response
     *
     * @return array
     */
    private function parseResponse(\stdClass $response)
    {
        $result = [];
        $resultResponse = [];
        if (property_exists($response, 'availableInstallmentPlans')) {
            foreach ($response->availableInstallmentPlans as $installmentPlan) {
                $result['value'] = $installmentPlan->installmentProfileNumber;
                $result['totalAmount'] = $this->currency->format(
                    $installmentPlan->totalAmount, null,
                    false
                );
                $result['installmentAmount'] = $this->currency->format(
                    $installmentPlan->installmentAmount,
                    null,
                    false
                );
                $result['numberOfInstallments'] = $installmentPlan->numberOfInstallments;
                $result['effectiveInterestRate'] = $installmentPlan->effectiveInterestRate;
                $result['interestRate'] = $installmentPlan->interestRate;
                $result['optionText'] = sprintf(__(
                    '%s per month in %s installments'),
                    $result['installmentAmount'],
                    $result['numberOfInstallments']
                );
                $resultResponse[] = $result;
            }
            return $resultResponse;
        }
        $resultResponse[] = ['error' => true];
        return $resultResponse;
    }
}