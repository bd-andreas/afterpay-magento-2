<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model\Order;

use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\InvoiceManagementInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Service\InvoiceService;

class Processor
{
    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var InvoiceManagementInterface|InvoiceService
     */
    private $invoiceService;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var TransactionFactory
     */
    private $transactionFactory;

    /**
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param InvoiceManagementInterface $invoiceService
     * @param TransactionFactory $transactionFactory
     * @param Registry $registry
     */
    public function __construct(
        InvoiceRepositoryInterface $invoiceRepository,
        InvoiceManagementInterface $invoiceService,
        TransactionFactory $transactionFactory,
        Registry $registry
    ) {
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceService = $invoiceService;
        $this->registry = $registry;
        $this->transactionFactory = $transactionFactory;
    }

    /**
     * @param OrderInterface|Order $order
     * @throws LocalizedException
     */
    public function capture(OrderInterface $order)
    {
        if (!$order->canInvoice()) {
            throw new LocalizedException(
                __('The order does not allow an invoice to be created.')
            );
        }
        $invoice = $this->invoiceService->prepareInvoice($order);

        if (!$invoice->getTotalQty()) {
            throw new LocalizedException(
                __('You can\'t create an invoice without products.')
            );
        }

        $this->registry->register('current_invoice', $invoice, true);
        $invoice->setData('requested_capture_case', Order\Invoice::CAPTURE_ONLINE);
        $invoice->register();
        $order->setData('is_in_process', true);

        $transactionSave = $this->transactionFactory->create();
        $transactionSave->addObject(
            $invoice
        )->addObject(
            $order
        )->save();
    }
}
