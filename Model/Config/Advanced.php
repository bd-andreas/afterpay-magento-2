<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model\Config;

use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Model\Config\Source\CaptureType;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Advanced
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function captureModeAuto($storeId = null)
    {
        return (int) $this->getValue('afterpay_capture/active', $storeId) === CaptureType::INVOICE_MODE_ENABLED;
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function captureModeShipping($storeId = null)
    {
        return (int) $this->getValue('afterpay_capture/active', $storeId) === CaptureType::INVOICE_MODE_SHIPPING;
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function captureModeDisabled($storeId = null)
    {
        return (int) $this->getValue('afterpay_capture/active', $storeId) === CaptureType::INVOICE_MODE_DISABLED;
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function captureModeManual($storeId = null)
    {
        return (int) $this->getValue('afterpay_capture/active', $storeId) === CaptureType::INVOICE_MODE_MANUAL;
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function invoiceNotCaptureActive($storeId = null)
    {
        return (int) $this->getValue('afterpay_capture/not_capture_active', $storeId) === Data::INVOICE_NOT_CAPTURE_ACTIVE;
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function captureModeStatus($storeId = null)
    {
        return (int) $this->getValue('afterpay_capture/active', $storeId) === CaptureType::INVOICE_MODE_STATUS;
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function autoCaptureOrderStatus($storeId = null)
    {
        return $this->getValue('afterpay_capture/auto_capture_status', $storeId);
    }

    /**
     * @param $field
     * @param int|null $storeId
     * @return string
     */
    private function getValue($field, $storeId = null)
    {
        if ($storeId !== null) {
            return $this->scopeConfig->getValue(sprintf('payment/%s', $field), ScopeInterface::SCOPE_STORES, $storeId);
        }
        return $this->scopeConfig->getValue(sprintf('payment/%s', $field));
    }
}
