<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class ConnectionTypeRest implements ArrayInterface
{
    const CONNECTION_TYPE_PRODUCTION = 0;
    const CONNECTION_TYPE_TEST = 1;
    const CONNECTION_TYPE_SANDBOX = 2;
    const CONNECTION_TYPE_PRODUCTION_LABEL = 'Production';
    const CONNECTION_TYPE_TEST_LABEL = 'Test';
    const CONNECTION_TYPE_SANDBOX_LABEL = 'Sandbox';

    /**
     * Connection mode options
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => self::CONNECTION_TYPE_PRODUCTION,
                'label' => __(self::CONNECTION_TYPE_PRODUCTION_LABEL)
            ],
            [
                'value' => self::CONNECTION_TYPE_TEST,
                'label' => __(self::CONNECTION_TYPE_TEST_LABEL)
            ],
            [
                'value' => self::CONNECTION_TYPE_SANDBOX,
                'label' => __(self::CONNECTION_TYPE_SANDBOX_LABEL)
            ]
        ];
    }
}
