<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model\Config\Source;

use \Magento\Sales\Model\Order\Config as OrderConfig;

class StatusesNew implements \Magento\Framework\Option\ArrayInterface
{
    
    const ORDER_STATE_NEW = 'new';
    
    /**
     * Order config
     */
    protected $_orderConfig;

    /**
     * StatusesNew constructor.
     * @param OrderConfig $_orderConfig
     */
    public function __construct(OrderConfig $_orderConfig)
    {
        $this->_orderConfig = $_orderConfig;
    }

    /**
     * Return order statuses
     */
    public function toOptionArray()
    {
        $result = [];
        
        $statuses = $this->_orderConfig->getStateStatuses(self::ORDER_STATE_NEW);

        foreach ($statuses as $code => $status) {
            $result[] = ['value' => $code, 'label' => $status];
        }
        
        return $result;
    }
}
