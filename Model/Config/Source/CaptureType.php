<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class CaptureType implements ArrayInterface
{
    const INVOICE_MODE_ENABLED = 1;
    const INVOICE_MODE_DISABLED = 2;
    const INVOICE_MODE_SHIPPING = 3;
    const INVOICE_MODE_STATUS = 4;
    const INVOICE_MODE_MANUAL = 0;

    /**
     * Return invoice mode options
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Manual')],
            ['value' => 1, 'label' => __('Yes, direct')],
            ['value' => 2, 'label' => __('Disabled')],
            ['value' => 3, 'label' => __('Yes, at shipping')],
            ['value' => 4, 'label' => __('Yes, at selected status')]
        ];
    }
}
