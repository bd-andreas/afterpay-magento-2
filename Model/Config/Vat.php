<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model\Config;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class Vat
{
    const XML_FIELD_VAT_CONFIG_1 = 'vat_1';
    const XML_FIELD_VAT_CONFIG_2 = 'vat_2';
    const XML_FIELD_VAT_CONFIG_3 = 'vat_3';
    const XML_FIELD_VAT_CONFIG_4 = 'vat_4';
    const XML_FIELD_VAT_CONFIG_5 = 'vat_5';
    const XML_FIELD_VAT_CONFIG_FALLBACK = 'vat_fallback';
    const XML_FIELD_VAT_CONFIG_SHIPPING = 'vat_shipping';
    const XML_FIELD_VAT_CONFIG_DISCOUNT = 'vat_discount';
    const XML_FIELD_VAT_CONFIG_FEE = 'vat_fee';
    const VAT_CONFIG_PATH = 'payment/afterpay_nl_digital_invoice/%s';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var array
     */
    private $vatSetup = [];

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ProductRepositoryInterface $productRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ProductRepositoryInterface $productRepository,
        LoggerInterface $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
    }

    /**
     * Get appropriate Afterpay VAT category for a given Magento product tax class ID or string (for discount/shipping)
     *
     * @param int
     * @param string|integer $identifier VAT category
     * @return integer
     */
    public function getAfterpayVATCategory($scope, $identifier)
    {
        if (empty($this->vatSetup)) {
            $this->initializeVatCategories($scope);
        }

        if (isset($this->vatSetup[$identifier])) {
            return $this->vatSetup[$identifier];
        }

        return $this->vatSetup['fallback'];
    }

    /**
     * Retrieve VAT setup from default payment method configuration. Create an array in the form of
     * [
     *     ['Tax class identifier OR product tax class ID' => 'Afterpay VAT Category ID']
     * ]
     *
     * @param int $storeId
     */
    private function initializeVatCategories($storeId)
    {
        // First set the fallback category
        $this->vatSetup['fallback'] = $this->getVATConfig(self::XML_FIELD_VAT_CONFIG_FALLBACK, $storeId);
        // Then go through rest of config, if set, add to config array
        if ($id = $this->getVATConfig(self::XML_FIELD_VAT_CONFIG_SHIPPING, $storeId)) {
            $this->vatSetup['shipping'] = $id;
        }
        if ($id = $this->getVATConfig(self::XML_FIELD_VAT_CONFIG_DISCOUNT, $storeId)) {
            $this->vatSetup['discount'] = $id;
        }
        if ($id = $this->getVATConfig(self::XML_FIELD_VAT_CONFIG_FEE, $storeId)) {
            $this->vatSetup['fee'] = $id;
        }
        // Product tax classes are configured as multiselect
        $productTaxClasses = [
            self::XML_FIELD_VAT_CONFIG_1 => 1,
            self::XML_FIELD_VAT_CONFIG_2 => 2,
            self::XML_FIELD_VAT_CONFIG_3 => 3,
            self::XML_FIELD_VAT_CONFIG_4 => 4,
            self::XML_FIELD_VAT_CONFIG_5 => 5
        ];
        foreach ($productTaxClasses as $configId => $afterpayId) {
            if ($ids = $this->getVATConfig($configId, $storeId)) {
                foreach (explode(',', $ids) as $id) {
                    $this->vatSetup[$id] = $afterpayId;
                }
            }
        }
    }

    /**
     * Read VAT config value by field id
     *
     * @param $field
     * @param null $store
     * @return string
     */
    private function getVATConfig($field, $store)
    {
        return $this->scopeConfig->getValue(
            sprintf(self::VAT_CONFIG_PATH, $field),
            ScopeInterface::SCOPE_STORES,
            $store
        );
    }

    /**
     * Load product from repo and get its tax ID
     *
     * @param int $id
     * @return int
     */
    public function getTaxIdByProduct($id)
    {
        try {
            return $this->productRepository->getById($id)->getTaxClassId();
        } catch (NoSuchEntityException $exception) {
            $this->logger->error($exception->getMessage());
        }
        return 1;
    }
}
