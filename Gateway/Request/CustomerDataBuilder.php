<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Afterpay\Payment\Helper\Service\Data;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Backend\Model\Session\Quote as QuoteSession;
use Magento\Framework\App\State;

/**
 * Class CustomerDataBuilder
 *
 * @package Afterpay\Payment\Gateway\Request
 */
class CustomerDataBuilder implements BuilderInterface
{
    /**
     * Genders
     */
    const GENDER_MALE = 1;

    const GENDER_FEMALE = 2;

    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $quoteSession;
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;
    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;
    /**
     * @var bool
     */
    protected $isBackend;


    /**
     * CustomerDataBuilder constructor.
     *
     * @param \Afterpay\Payment\Helper\Service\Data $serviceHelperFactory
     * @param \Magento\Checkout\Model\Session       $checkoutSession
     * @param \Magento\Backend\Model\Session\Quote  $quoteSession
     * @param \Magento\Framework\App\State          $appState
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        Data $serviceHelperFactory,
        CheckoutSession $checkoutSession,
        QuoteSession $quoteSession,
        State $appState
    ) {
    
        $this->helper = $serviceHelperFactory;
        if ($appState->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
            $this->isBackend = true;
            $this->quote = $quoteSession->getQuote();
        } else {
            $this->isBackend = false;
            $this->quote = $checkoutSession->getQuote();
        }
        $this->checkoutSession = $checkoutSession;
        $this->quoteSession = $quoteSession;
        $this->appState = $appState;
    }

    /**
     * @param array $buildSubject
     *
     * @return array
     * @throws \Exception
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $buildSubject['payment'];
        $order = $paymentDO->getOrder();
        $payment = $paymentDO->getPayment();
        $customer = $this->quote->getCustomer();
        $additionalInfo = $payment->getAdditionalInformation();
        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress() ?: $billingAddress;

        $gender = $this->helper->readAdditionalInfo($additionalInfo['additional_data'], 'customer_gender');
        // Use customers gender if it is set, use payment method gender otherwise
        $gender = ($customer->getGender() > 0 && $customer->getGender() < 3) ? $customer->getGender() : $gender;
        // Use payment method phone if set, customer address phone otherwise
        $customerTelephone = $this->helper->readAdditionalInfo($additionalInfo['additional_data'], 'customer_telephone');
        // Use customers dob if it is set, use payment method dob otherwise
        $dob = $customer->getDob() ?: $this->helper->readAdditionalInfo($additionalInfo['additional_data'], 'customer_dob');
        $dob = $this->helper->formatDob($dob);
        $customerSsn = $this->helper->readAdditionalInfo($additionalInfo['additional_data'], 'ssn');
        // Use customer Social Security Number only if it is present
        switch ($gender) {
            case self::GENDER_MALE:
                $gender = 'M';
                break;
            case self::GENDER_FEMALE:
                $gender = 'V';
                break;
            default:
                $gender = '';
                break;
        }
        return [
            'billtoaddress' => [
                'referenceperson' =>
                    [
                        'dob' => $dob,
                        'email' => $billingAddress->getEmail(),
                        'gender' => $gender,
                        'initials' => $this->helper->getInitials($billingAddress->getFirstname()),
                        'firstname' => $billingAddress->getFirstname(),
                        'isolanguage' => $this->helper->getIsoLanguage($payment->getMethod()),
                        'lastname' => $billingAddress->getLastname(),
                        'phonenumber' => $customerTelephone ?: $billingAddress->getTelephone(),
                        'ssn' => $customerSsn
                    ]
            ],
            'shiptoaddress' => [
                'referenceperson' =>
                    [
                        'dob' => $dob,
                        'email' => $shippingAddress->getEmail(),
                        'gender' => $gender,
                        'initials' => $this->helper->getInitials($billingAddress->getFirstname()),
                        'firstname' => $shippingAddress->getFirstname(),
                        'isolanguage' => $this->helper->getIsoLanguage($payment->getMethod()),
                        'lastname' => $shippingAddress->getLastname(),
                        'phonenumber' => $customerTelephone ?: $shippingAddress->getTelephone(),
                        'ssn' => $customerSsn
                    ]
            ]
        ];
    }
}
