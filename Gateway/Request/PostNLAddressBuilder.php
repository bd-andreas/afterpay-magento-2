<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Module\Manager;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\AddressFactory;

class PostNLAddressBuilder implements BuilderInterface
{
    const PG_ADDRESS_TYPE = 'pakjegemak';

    /**
     * @var Manager
     */
    private $manager;
    /**
     * @var AddressFactory
     */
    private $addressFactory;

    /**
     * @param Manager $manager
     * @param AddressFactory $addressFactory
     */
    public function __construct(
        Manager $manager,
        AddressFactory $addressFactory
    ) {
        $this->manager = $manager;
        $this->addressFactory = $addressFactory;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $buildSubject['payment'];
        $order = $paymentDO->getPayment()->getOrder();
        $data = [];

        if ($this->manager->isEnabled('TIG_PostNL')) {
            $pickupAddress = $this->getPostNLPickupAddress($order->getQuoteId());
            if ($pickupAddress->getId()) {
                $data['shiptoaddress']['city'] = $pickupAddress->getCity();
                $data['shiptoaddress']['streetname'] = implode(' ', $pickupAddress->getStreet());
                $data['shiptoaddress']['postalcode'] = $pickupAddress->getPostcode();
                $data['shiptoaddress']['referenceperson']['firstname'] = 'A';
                $data['shiptoaddress']['referenceperson']['initials'] = 'A';
                $data['shiptoaddress']['referenceperson']['lastname'] = sprintf(
                    'POSTNL Afhaalpunt %s',
                    $pickupAddress->getCompany()
                );
            }
        }

        return $data;
    }

    /**
     * If PostNL module enabled, check if pickup order
     * In such case  return quote address with pickup location data for further use
     *
     * Doing here because PostNL module has not yet saved data on order
     *
     * @param int $quoteId
     * @return Address|DataObject
     */
    private function getPostNLPickupAddress($quoteId)
    {
        $quoteAddress = $this->addressFactory->create();

        // there is no repository or address collection for quotes
        $collection = $quoteAddress->getCollection();
        $collection->addFieldToFilter('quote_id', $quoteId);
        $collection->addFieldToFilter('address_type', self::PG_ADDRESS_TYPE);

        return $collection->setPageSize(1)->getFirstItem();
    }
}
