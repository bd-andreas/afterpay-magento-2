<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Request;

use Afterpay\Payment\Model\Config\Vat;
use Magento\Bundle\Model\Product\Price;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Framework\App\RequestInterface;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Creditmemo\Item;
use Magento\Sales\Model\Order\Payment;

class RefundDataBuilder implements BuilderInterface
{
    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var Vat
     */
    private $vatHelper;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * Constructor
     *
     * @param SubjectReader $subjectReader
     * @param Vat $vatHelper
     * @param RequestInterface $request
     */
    public function __construct(
        SubjectReader $subjectReader,
        Vat $vatHelper,
        RequestInterface $request
    ) {
        $this->subjectReader = $subjectReader;
        $this->vatHelper = $vatHelper;
        $this->request = $request;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        /** @var Payment $payment */
        $payment = $paymentDO->getPayment();

        return $this->gatherRefundData($payment);
    }

    /**
     * Gathering required information for Afterpay
     *
     * @param Payment $payment
     * @return array
     */
    private function gatherRefundData($payment)
    {
        $creditmemo = $payment->getCreditmemo();
        if (!$creditmemo) {
            throw new \InvalidArgumentException(
                'Invalid Creditmemo object for order ID: %1',
                $payment->getOrder()->getIncrementId()
            );
        }
        // attach the value from checkbox in form to creditmemo object for use in other methods
        if (isset($this->request->getParam('creditmemo')['refund_afterpay_fee'])) {
            $creditmemo->setData(
                'afterpay_service_tax_refund',
                $this->request->getParam('creditmemo')['refund_afterpay_fee']
            );
        }

        $invoice = $creditmemo->getInvoice();
        $order = $payment->getOrder();

        $result = [
            'order_country' => $order->getBillingAddress()->getCountryId(),
            'invoicenumber' => $invoice->getIncrementId(),
            'ordernumber' => $order->getIncrementId(),
            'creditinvoicenumber' => $creditmemo->getIncrementId(),
            'payment' => $payment
        ];

        foreach ($creditmemo->getAllItems() as $item) {
            if ($this->validForSubmit($item)) {
                $result['orderlines'][] = $this->prepareOrderLine($item);
            }
        }

        if ($creditmemo->getAdjustment()) {
            $result['orderlines'][] = $this->prepareAdjustmentLine($creditmemo, $order);
        }
        if ($creditmemo->getAfterpayServiceTaxRefund()) {
            $afterpayPaymentFee = $order->getAfterpayPaymentFee();
            $result['orderlines'][] = $this->prepareFeeLine($afterpayPaymentFee, $order);
        }
        if ($creditmemo->getDiscountAmount() < 0) {
            $result['orderlines'][] = $this->prepareDiscountLine($creditmemo, $order);
        }
        if ($creditmemo->getShippingInclTax() > 0) {
            $result['orderlines'][] = $this->prepareShippingFeeLine($creditmemo->getShippingInclTax(), $order);
        }

        return $result;
    }

    /**
     * @param Item $item
     * @return bool
     */
    private function validForSubmit($item): bool
    {
        return ((float) $item->getPriceInclTax() > 0 || (float) $item->getPriceInclTax() < 0)
            && !$item->getParentId()
            && $item->getQty()
            && !$this->itemIsDynamicPriceBundle($item);
    }

    /**
     * @param Item $item
     * @return bool
     */
    private function itemIsDynamicPriceBundle($item): bool
    {
        return $item->getOrderItem()->getProductType() === ProductType::TYPE_BUNDLE
            && $item->getOrderItem()->getProduct()->getPriceType() === Price::PRICE_TYPE_DYNAMIC;
    }

    /**
     * Add order line to service object
     *
     * @param Item $item
     * @return array
     */
    private function prepareOrderLine($item): array
    {
        $taxClassId = $this->vatHelper->getTaxIdByProduct($item->getProductId());
        $vatAmount = $item->getTaxAmount();
        if ($item->getDiscountAmount() > 0.00) {
            $vatAmount = $item->getBaseRowTotal() * $item->getOrderItem()->getTaxPercent() / 100;
        }
        return [
            'sku' => $item->getSku(),
            'name' => __('Refund:') . ' ' . $item->getName(),
            'qty' => $item->getQty(),
            'price' => (int) round($item->getPriceInclTax() * 100 * -1, 0),
            'taxCategory' => $this->vatHelper->getAfterpayVATCategory($item->getStoreId(), $taxClassId),
            'taxAmount' => $vatAmount,
        ];
    }

    /**
     * @param Order\Creditmemo $creditmemo
     * @param  $order
     *
     * @return array
     */
    private function prepareAdjustmentLine($creditmemo, $order): array
    {
        $adjustment = $creditmemo->getAdjustment();
        if ($creditmemo->getAfterpayServiceTaxRefund()) {
            $afterpayFee = $order->getAfterpayPaymentFee();
            $adjustmentToAdd = $adjustment - $afterpayFee;
        } else {
            $adjustmentToAdd = $adjustment;
        }

        return [
            'sku' => 'ADJUSTMENT',
            'name' => 'Adjustment',
            'qty' => 1,
            'price' => (string) ($adjustmentToAdd * 100 * -1),
            'taxCategory' => '4',
        ];
    }

    /**
     * Add service fee line to service object
     *
     * @param string $paymentFee
     * @param Order $order
     * @return array
     */
    private function prepareFeeLine($paymentFee, $order): array
    {
        return [
            'sku' => 'FEE',
            'name' => 'Refund: Payment Fee',
            'qty' => '1',
            'price' => (string) $paymentFee * -100,
            'taxCategory' => $this->vatHelper->getAfterpayVATCategory($order->getStoreId(), 'fee'),
        ];
    }

    /**
     * Add discount line to service object
     *
     * @param Order\Creditmemo $creditmemo
     * @param Order $order
     *
     * @return array
     */
    private function prepareDiscountLine($creditmemo, $order): array
    {
        return [
            'sku' => 'DISCOUNT',
            'name' => 'Refund: Discount',
            'qty' => '1',
            'price' => (string) (abs($creditmemo->getDiscountAmount()) * 100),
            'taxCategory' => $this->vatHelper->getAfterpayVATCategory($order->getStoreId(), 'discount'),
            'taxAmount' => $creditmemo->getDiscountTaxCompensationAmount() * -1
        ];
    }

    /**
     * Add shipping fee line
     *
     * @param float $shippingFee
     * @param Order $order
     * @return array
     */
    private function prepareShippingFeeLine($shippingFee, $order): array
    {
        $feeInCents = $shippingFee * 100 * -1;
        return [
            'sku' => 'SHIPPING',
            'name' => 'Refund: Shipping',
            'qty' => '1',
            'price' => (string) $feeInCents,
            'taxCategory' => $this->vatHelper->getAfterpayVATCategory($order->getStoreId(), 'shipping'),
            'taxAmount' => $order->getShippingTaxAmount()
        ];
    }
}
