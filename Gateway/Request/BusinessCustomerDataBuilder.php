<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;

class BusinessCustomerDataBuilder implements BuilderInterface
{
    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $buildSubject['payment'];
        $additionalInfo = $paymentDO->getPayment()->getAdditionalInformation()['additional_data'];

        return [
            'company' => [
                'cocnumber' => $additionalInfo['coc_number'],
                'companyname' => $additionalInfo['company_name'],
            ],
            # B2B customers can not have dob yet service expects it to be valid date
            'billtoaddress' => ['referenceperson' => ['dob' => '1970-01-01T00:00:00']],
            'shiptoaddress' => ['referenceperson' => ['dob' => '1970-01-01T00:00:00']]
        ];
    }
}
