<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Request;

use Magento\Checkout\Model\Session;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Module\Manager;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\AddressFactory;
use Afterpay\Payment\Helper\Service\Data;

class UPSAPAddressBuilder implements BuilderInterface
{
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var Manager
     */
    private $manager;
    /**
     * @var AddressFactory
     */
    private $addressFactory;

    /**
     * @param Manager $manager
     * @param AddressFactory $addressFactory
     * @param Session $session
     * @param Data $helper
     */
    public function __construct(
        Manager $manager,
        AddressFactory $addressFactory,
        Session $session,
        Data $helper
    )
    {
        $this->manager = $manager;
        $this->addressFactory = $addressFactory;
        $this->session = $session;
        $this->helper = $helper;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $buildSubject['payment'];
        $order = $paymentDO->getPayment()->getOrder();
        $data = [];
        if ($this->manager->isEnabled('Infomodus_Upsap')) {
            if ($this->helper->getStoreConfig('carriers/upsap/active', $order->getStoreId())) {
                if ($this->session->getUpsapAddLine1()) {
                    $address = implode(' ',
                        [
                            $this->session->getUpsapAddLine1(),
                            $this->session->getUpsapAddLine2(),
                            $this->session->getUpsapAddLine3()
                        ]
                    );
                    $address = $this->helper->getSplitStreet($address);
                    $data = [
                        'shiptoaddress' => [
                            'isocountrycode' => $this->session->getUpsapCountry(),
                            'city' => $this->session->getUpsapCity(),
                            'postalcode' => $this->session->getUpsapPostal(),
                            'streetname' => $address['streetname'],
                            'housenumber' => $address['housenumber'],
                            'housenumberaddition' => $address['houseNumberAddition'],
                            'referenceperson' =>
                                [
                                    'initials' => 'A',
                                    'firstname' => 'A',
                                    'lastname' => 'UPS '.$this->session->getUpsapName(),
                                ]
                        ]
                    ];
                }
            }
        }
        return $data;
    }


}
