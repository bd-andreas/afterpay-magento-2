<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\Exception\LocalizedException;

class AddressDataBuilder implements BuilderInterface
{
    /**
     * @param array $buildSubject
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $buildSubject['payment'];
        $order = $paymentDO->getOrder();
        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress() ?: $billingAddress;
        $addresses = $paymentDO->getPayment()->getOrder()->getAddresses();
        $splitBillingAddress = $this->getSplitStreet(
            implode(' ', $addresses[0]->getStreet())
        );
        if (array_key_exists(1, $paymentDO->getPayment()->getOrder()->getAddresses())) {
            $splitShippingAddress = $this->getSplitStreet(
                implode(' ', $addresses[1]->getStreet())
            );
        } else {
            $splitShippingAddress = $splitBillingAddress;
        }
        $result = [
            'billtoaddress' => [
                'isocountrycode' => $billingAddress->getCountryId(),
                'city' => $billingAddress->getCity(),
                'postalcode' => $billingAddress->getPostcode(),
                'streetname' => $splitBillingAddress['streetname'],
                'housenumber' => $splitBillingAddress['housenumber'],
                'housenumberaddition' => $splitBillingAddress['houseNumberAddition']
            ],
            'shiptoaddress' => [
                'isocountrycode' => $shippingAddress->getCountryId(),
                'city' => $shippingAddress->getCity(),
                'postalcode' => $shippingAddress->getPostcode(),
                'streetname' => $splitShippingAddress['streetname'],
                'housenumber' => $splitShippingAddress['housenumber'],
                'housenumberaddition' => $splitShippingAddress['houseNumberAddition'],
            ]
        ];

        return $result;
    }

    /**
     * Split address
     *
     * @param $address
     *
     * @return array
     * @throws LocalizedException
     */
    protected function getSplitStreet($address)
    {
        $address = is_array($address) ? implode($address, ' ') : $address;
        $ret = [
            'streetname' => '',
            'housenumber' => '',
            'houseNumberAddition' => '',
        ];

        if (preg_match('/^(.*?)([0-9]+)(.*)/s', $address, $matches)) {
            if ('' == $matches[1]) {
                // Number at beginning
                $ret['housenumber'] = trim($matches[2]);
                $ret['streetname'] = trim($matches[3]);
            } else {
                // Number at end
                $ret['streetname'] = trim($matches[1]);
                $ret['housenumber'] = trim($matches[2]);
                $ret['houseNumberAddition'] = trim($matches[3]);
            }
        } else {
            $message = 'The house number addition of the shipping address is missing. 
            Please check your shipping details or contact our customer service.';
            throw new LocalizedException(__($message));
        }

        return $ret;
    }
}
