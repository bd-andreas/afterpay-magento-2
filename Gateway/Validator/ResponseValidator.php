<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Validator;

use Afterpay\Payment\Model\Config\Advanced;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Validator\AbstractValidator;
use Magento\Payment\Gateway\Validator\ResultInterface;
use Magento\Payment\Gateway\Validator\ResultInterfaceFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\StatusResolver;
use Magento\Store\Model\ScopeInterface;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;

class ResponseValidator extends AbstractValidator
{
    const RESULT_SUCCESS = 0;
    const RESULT_REFUSED = 3;
    const CONFIG_ORDER_STATUS_REFUSED = 'payment/afterpay_capture/order_status_refused';
    const CONFIG_CAPTURE_ACTIVE = 'payment/afterpay_capture/active';

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StatusResolver
     */
    private $statusResolver;

    /**
     * @var DebugHelper
     */
    protected $debugHelper;

    /**
     * @var Advanced
     */
    private $advancedConfig;

    /**
     * @var ResultInterfaceFactory
     */
    private $resultFactory;

    /**
     * @var array
     */
    private $invalidAddressCodes = ['200.101', '200.103', '200.104'];

    /**
     * @param ResultInterfaceFactory $resultFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param ScopeConfigInterface $scopeConfig
     * @param StatusResolver $statusResolver
     */
    public function __construct(
        ResultInterfaceFactory $resultFactory,
        OrderRepositoryInterface $orderRepository,
        ScopeConfigInterface $scopeConfig,
        StatusResolver $statusResolver,
        DebugHelper $debugHelper,
        Advanced $advancedConfig
    ) {
        parent::__construct($resultFactory);
        $this->orderRepository = $orderRepository;
        $this->scopeConfig = $scopeConfig;
        $this->statusResolver = $statusResolver;
        $this->debugHelper = $debugHelper;
        $this->advancedConfig = $advancedConfig;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Performs domain-related validation for business object
     *
     * @param array $validationSubject
     * @return ResultInterface
     */
    public function validate(array $validationSubject)
    {
        $response = SubjectReader::readResponse($validationSubject);
        $paymentDO = SubjectReader::readPayment($validationSubject);
        $order = $paymentDO->getPayment()->getOrder();

        $isValid = true;
        $fails = [];

        $statements = [
            [
                (int) ($response['object']->resultId ?? self::RESULT_REFUSED) === self::RESULT_SUCCESS,
                __($this->extractErrorFromResponse($response))
            ]
        ];

        foreach ($statements as $statementResult) {
            if (!$statementResult[0]) {
                $isValid = false;
                $fails[] = $statementResult[1];

                $this->persistFail($order, $statementResult[1]);

                $this->debugHelper->debug(
                    $order->getPayment()->getMethod(),
                    [
                        'text' => $statementResult[1]->getText(),
                    ],
                    true
                );
            }
        }

        return $this->createResult($isValid, $fails);
    }

    /**
     * If there is a message in response, use that
     *
     * @param $response object|array
     *
     * @return string
     */
    private function extractErrorFromResponse($response)
    {
        if (isset($response['object']->riskCheckMessages)) {
            if (
                isset($response['object']->customer->addressList[0]) &&
                in_array($response['object']->riskCheckMessages[0]->code, $this->invalidAddressCodes, true)
            ) {
                $address = $response['object']->customer->addressList[0];
                $errorMessage = sprintf(
                    'Leider ist uns die eingegebene Rechnungsadresse nicht bekannt, jedoch wurde eine 
                    mögliche, abweichende Rechnungsadresse ermittelt. Sollte sie korrekt sein, bitten wir, den 
                    Bestellvorgang mit dieser Adresse noch einmal durchzuführen. %s %s %s, %s',
                    $address->street, $address->streetNumber, $address->postalPlace, $address->postalCode
                );
                return $errorMessage;
            }

            $messages = $response['object']->riskCheckMessages;

            return $messages[0]->customerFacingMessage;
        }

        // TODO this seems to be soap specific, consider separating
        if (isset($response['object']->failures)) {
            if (isset($response['object']->failures->messages)) {
                $messages = $response['object']->failures->messages;
                return $messages[0]->message;
            }
        }

        // TODO this seems to be soap specific, consider separating
        if (isset($response['object']->failures)) {
            if (isset($response['object']->messages)) {
                $messages = $response['object']->messages;
                return $messages[0]->description;
            }
        }

        // TODO this seems to be soap specific, consider separating
        if (isset($response['object']->rejectDescription)) {
            if (isset($response['object']->messages->description)) {
                return $response['object']->messages->description;
            }
        }

        return 'Request has been rejected.';
    }

    /**
     * Load NEW order and persist that to save custom status.
     * Can not use the existing because that has items invoiced
     *
     * @param Order $order
     * @param string $message
     */
    private function persistFail($order, $message)
    {
        if ($this->shouldSkip($order)) {
            return;
        }

        $freshOrder = $this->orderRepository->get($order->getId());
        $freshOrder->addStatusHistoryComment(
            __('Afterpay capture refused: %1', $message),
            $this->loadOrderStatus($order)
        );
        $this->orderRepository->save($freshOrder);
    }

    /**
     * @param Order $order
     * @return bool
     */
    private function shouldSkip($order)
    {
        $autoCapture = $this->advancedConfig->captureModeAuto($order->getStoreId());
        $beingAuthorized = (int) $order->getAfterpayCaptured() !== 1;

        return !$this->loadOrderStatus($order) || $autoCapture || $beingAuthorized;
    }

    /**
     * We only want to set specific status if such is configured
     *
     * @param Order $order
     * @return string|null
     */
    private function loadOrderStatus($order)
    {
        $current = $order->getStatus();
        $default = $this->statusResolver->getOrderStatusByState($order, Order::STATE_PROCESSING);
        $configured =  $this->scopeConfig->getValue(
            self::CONFIG_ORDER_STATUS_REFUSED,
            ScopeInterface::SCOPE_STORES,
            $order->getStoreId()
        );

        // when theres nothing to change
        if ($current === $configured || $configured === $default) {
            return false;
        }

        return $configured;
    }
}
