<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Validator;

use Afterpay\AfterpayFactory;
use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Gateway\Http\Client\CaptureSale;
use Magento\Payment\Gateway\Validator\{AbstractValidator, ResultInterfaceFactory};
use Magento\Payment\Gateway\ConfigInterface;

class GlobalValidator extends AbstractValidator
{
    private $config;
    /**
     * @var Afterpay
     */

    private $afterpay;
    /**
     * @var Data
     */
    private $helper;

    /**
     * GlobalValidator constructor.
     *
     * @param ResultInterfaceFactory $resultFactory
     * @param ConfigInterface $config
     * @param Afterpay $afterpay
     * @param Data $helper
     */
    public function __construct(
        ResultInterfaceFactory $resultFactory,
        ConfigInterface $config,
        AfterpayFactory $afterpay,
        Data $helper
    ) {
        $this->config = $config;
        $this->afterpay = $afterpay->create();
        $this->helper = $helper;
        parent::__construct($resultFactory);
    }

    /**
     * @param array $validationSubject
     *
     * @return \Magento\Payment\Gateway\Validator\ResultInterface
     */
    public function validate(array $validationSubject)
    {
        $isValid = true;

        $payment = $validationSubject['payment'];
        $additionalInformation = $payment->getAdditionalInformation();
        $bankAccount = $this->helper->readAdditionalInfo($additionalInformation['additional_data'], 'bankaccountnumber');
        $bankCode = $this->helper->readAdditionalInfo($additionalInformation['additional_data'], 'bankCode');
        if ($bankCode && $bankAccount) {
            $data = [
                'bankAccount' => $bankAccount,
                'bankCode' => $bankCode,
            ];
            $auth = $this->getConfiguration();
            $this->afterpay->setRest();
            $this->afterpay->set_ordermanagement('validate_bankaccount');
            $this->afterpay->set_order($data, CaptureSale::ORDER_MANAGEMENT_CODE);
            $this->afterpay->do_request($auth, $auth['mode']);
            if (!$this->afterpay->order_result->return->isValid) {
                $isValid = false;
            }
        }

        return $this->createResult($isValid);
    }

    private function getConfiguration()
    {
        $connectionType = $this->config->getValue('testmode');
        $result['modus'] = $this->helper->getConnectionType($connectionType);
        $result['mode'] = $this->helper->getConnectionType($connectionType, true);
        $result['apiKey'] = $this->config->getValue($result['modus'] . '_api_key');

        return $result;
    }
}
