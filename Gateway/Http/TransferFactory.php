<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Http;

use Magento\Config\Model\Config\Backend\Encrypted;
use Magento\Payment\Gateway\Config\ValueHandlerInterface;
use Magento\Payment\Gateway\Http\{TransferBuilder, TransferFactoryInterface, TransferInterface};
use Afterpay\Payment\Helper\Service\Data;

class TransferFactory implements TransferFactoryInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var TransferBuilder
     */
    private $transferBuilder;

    /**
     * @var ValueHandlerInterface
     */
    private $configValueHandler;

    /**
     * @var Encrypted
     */
    private $encrypted;

    /**
     * @param Encrypted $encrypted
     * @param TransferBuilder $transferBuilder
     * @param ValueHandlerInterface $configValueHandler
     * @param Data $helper
     */
    public function __construct(
        Encrypted $encrypted,
        TransferBuilder $transferBuilder,
        ValueHandlerInterface $configValueHandler,
        Data $helper
    ) {
        $this->transferBuilder = $transferBuilder;
        $this->configValueHandler = $configValueHandler;
        $this->encrypted = $encrypted;
        $this->helper = $helper;
    }

    /**
     * @inheritdoc
     */
    public function create(array $request): TransferInterface
    {
        $storeId = $request['payment']->getOrder()->getStoreId();
        return $this->transferBuilder
            ->setBody($request)
            ->setClientConfig($this->loadClientConfig($storeId))
            ->build();
    }

    /**
     * Handle weird/complicated mapping between field and mode names and return auth credentials from config
     *
     * @param null $storeId
     *
     * @return array
     */
    private function loadClientConfig($storeId =  null)
    {
        $connectionType = $this->configValueHandler->handle(['field' => 'testmode'], $storeId);
        $fieldsMap = [
            'merchantid' => '_merchant_id',
            'portfolioid' => '_portfolio_id',
            'apiKey' => '_api_key'
        ];

        $result = [
            'modus' => $this->helper->getConnectionType($connectionType, true),
            'password' => $this->loadPassword($connectionType, $storeId),
            'has_rest' => (bool) $this->configValueHandler->handle(['field' => 'has_rest'], $storeId)
        ];

        foreach ($fieldsMap as $key => $value) {
            $result[$key] = $this->configValueHandler->handle(
                ['field' => $this->helper->getConnectionType($connectionType) . $value],
                $storeId
            );
        }

        return $result;
    }

    /**
     * @param $connectionType
     * @param null $storeId
     *
     * @return string
     */
    private function loadPassword($connectionType, $storeId = null): string
    {
        return $this->encrypted->processValue(
            $this->configValueHandler->handle(
                ['field' => $this->helper->getConnectionType($connectionType) . '_password'],
                $storeId
            )
        );
    }
}
