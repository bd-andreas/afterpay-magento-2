<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Http\Client;

use Afterpay\AfterpayFactory;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;
use Magento\Payment\Model\Method\Logger;
use Psr\Log\LoggerInterface;

class RefundSale extends AbstractTransaction
{
    const ORDER_MANAGEMENT_CODE = 'OM';

    /**
     * Debug
     *
     * @var DebugHelper
     */
    protected $debugHelper;

    /**
     * @param LoggerInterface $logger
     * @param AfterpayFactory $afterpayFactory
     * @param Logger $customLogger
     * @param DebugHelper $debugHelper
     */
    public function __construct(
        LoggerInterface $logger,
        AfterpayFactory $afterpayFactory,
        Logger $customLogger,
        DebugHelper $debugHelper
    ) {
        parent::__construct($logger, $customLogger, $afterpayFactory);
        $this->debugHelper = $debugHelper;
    }

    /**
     * @inheritdoc
     */
    public function execute(array $data, array $clientConfig)
    {
        if ($this->isRestOrder($clientConfig)) {
            $this->afterpay->setRest();
        }

        $this->afterpay->setOrderCountry($data['order_country']);
        $this->afterpay->set_ordermanagement($data['refund_type']);
        unset($data['order_country']);

        foreach ($data['orderlines'] as $line) {
            $this->afterpay->create_order_line(...array_values($line));
        }

        # Need to set refund type AFTER lines
        $this->afterpay->set_order($data, self::ORDER_MANAGEMENT_CODE);

        $this->afterpay->do_request(
            $clientConfig,
            $clientConfig['modus']
        );

        return $this->afterpay->order_result->return;
    }
}
