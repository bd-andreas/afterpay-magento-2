<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Http\Client;

use Afterpay\AfterpayFactory;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;
use Magento\Payment\Model\Method\Logger;
use Psr\Log\LoggerInterface;

/**
 * Class TransactionSale
 */
class CaptureSale extends AbstractTransaction
{
    const ORDER_MANAGEMENT_CODE = 'OM';

    /**
     * Debug
     *
     * @var DebugHelper
     */
    protected $debugHelper;

    /**
     * TransactionSale constructor.
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Payment\Model\Method\Logger $customLogger
     * @param AfterpayFactory $afterpayFactory
     * @param DebugHelper $debugHelper
     */
    public function __construct(
        LoggerInterface $logger,
        Logger $customLogger,
        AfterpayFactory $afterpayFactory,
        DebugHelper $debugHelper
    ) {
        parent::__construct($logger, $customLogger, $afterpayFactory);
        $this->debugHelper = $debugHelper;
    }

    /**
     * @inheritdoc
     */
    protected function execute(array $data, array $clientConfig)
    {
        if ($this->isRestOrder($clientConfig)) {
            $this->afterpay->setRest();
        } else {
            unset($data['totalamount']);
        }

        $this->afterpay->set_ordermanagement('capture_full');
        $this->afterpay->set_order($data, self::ORDER_MANAGEMENT_CODE);
        $this->afterpay->do_request(
            $clientConfig,
            $clientConfig['modus']
        );

        return $this->afterpay->order_result->return;
    }
}
