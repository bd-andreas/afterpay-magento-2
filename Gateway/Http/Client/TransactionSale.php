<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Http\Client;

use Afterpay\AfterpayFactory;
use Afterpay\Payment\Model\Config\Vat as VatConfig;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;
use Magento\Backend\Model\Session\Quote as BackendCheckoutSession;
use Magento\Bundle\Model\Product\Price;
use Magento\Catalog\Model\Product\Type;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\{Area, State};
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Model\Method\Logger;
use Magento\Quote\Model\Quote\Item;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class TransactionSale
 *
 * @package Afterpay\Payment\Gateway\Http\Client
 */
class TransactionSale extends AbstractTransaction
{
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var State
     */
    protected $appState;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $order;

    /**
     * @var VatConfig
     */
    private $vatConfig;

    /**
     * Debug
     *
     * @var
     */
    protected $debugHelper;

    /**
     * @param LoggerInterface $logger
     * @param Logger $customLogger
     * @param AfterpayFactory $afterpayFactory
     * @param CheckoutSession $checkoutSession
     * @param BackendCheckoutSession $backendCheckoutSession
     * @param State $appState
     * @param OrderRepositoryInterface $orderRepository
     * @param VatConfig $vatConfig
     * @param DebugHelper $debugHelper
     *
     * @throws LocalizedException
     */
    public function __construct(
        LoggerInterface $logger,
        Logger $customLogger,
        AfterpayFactory $afterpayFactory,
        CheckoutSession $checkoutSession,
        BackendCheckoutSession $backendCheckoutSession,
        State $appState,
        OrderRepositoryInterface $orderRepository,
        VatConfig $vatConfig,
        DebugHelper $debugHelper
    ) {
        parent::__construct($logger, $customLogger, $afterpayFactory);
        $this->appState = $appState;
        # TODO checkoutSession must not be necessary here
        $this->checkoutSession =
            ($this->appState->getAreaCode() === Area::AREA_ADMINHTML) ? $backendCheckoutSession : $checkoutSession;
        $this->orderRepository = $orderRepository;
        $this->vatConfig = $vatConfig;
        $this->debugHelper = $debugHelper;
    }

    /**
     * @inheritdoc
     */
    protected function execute(array $data, array $clientConfig): \stdClass
    {

        $orderType = $this->checkoutSession->getQuote()->getPayment()->getMethod()
            === 'afterpay_nl_business_2_business' ? 'B2B' : 'B2C';

        // needs to be done before setting data on afterpay object
        if ($this->isRestOrder($clientConfig)) {
            $this->afterpay->setRest();
        }

        // TODO move these in separate builders
        $this->addProductLine();
        $this->addDiscountLine();
        $this->addShippingFeeLine();
        $this->addPaymentFeeLine();
        $this->addFoomanTotalLines();

        $this->afterpay->set_order($data, $orderType);
        $this->afterpay->do_request(
            $clientConfig,
            $clientConfig['modus']
        );

        return $this->afterpay->order_result->return;
    }

    /**
     * Add product line
     */
    private function addProductLine()
    {
        $quoteItems = $this->checkoutSession->getQuote()->getAllItems();
        /* @var Item $item */
        foreach ($quoteItems as $item) {
            if ($this->shouldBeSeparateLine($item)) {
                $vatAmount = $item->getTaxAmount();
                if ($item->getDiscountAmount() > 0.00) {
                    $vatAmount = $item->getBaseRowTotal() * $item->getTaxPercent() / 100;
                }
                $this->afterpay->create_order_line(
                    $item->getSku(), // Article ID
                    $item->getQty() . ' x ' . $item->getName(), // Article description
                    '1', // Quantity
                    (string) ($item->getRowTotalInclTax() * 100), // Unit price
                    $this->vatConfig->getAfterpayVATCategory(
                        $this->checkoutSession->getQuote()->getStoreId(),
                        $item->getTaxClassId()
                    ), // VAT category
                    $vatAmount, //VAT amount for product
                    null,
                    null,
                    $item->getProduct()->getProductUrl(),
                    $item->getAfterpayProductImage()
                );
            }
        }
    }

    /**
     * Determine whether quote item should be converted into order line. If item is not configurable child product or
     * bundle parent product with price 0.00 or dynamic price type, it will be added to order.
     *
     * @param Item $item
     * @return bool
     */
    private function shouldBeSeparateLine(Item $item): bool
    {
        if ($parent = $item->getParentItem()) {
            if ($parent->getProduct()->getTypeId() !== Type::TYPE_BUNDLE) {
                return false;
            }
        } elseif ($item->getProduct()->getTypeId() === Type::TYPE_BUNDLE) {
            if (
                $item->getProduct()->getPriceType() === Price::PRICE_TYPE_DYNAMIC
                || $item->getProduct()->getFinalPrice() === 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add discount line
     */
    private function addDiscountLine()
    {
        $discountAmountItems = 0;
        $discountAmountCompensation = 0;
        $quoteItems = $this->checkoutSession->getQuote()->getAllItems();
        foreach ($quoteItems as $item) {
            $discountAmountItems += $item->getDiscountAmount();
            $discountAmountCompensation += $item->getDiscountTaxCompensationAmount();
        }
        // discount is supposed to be negative
        $discountAmount = -1 * ($discountAmountItems * 100);
        if ($this->isRest()) {
            $discountAmountCompensation *= -1;
        }
        // TODO: Implement a better way todo this
        if ($discountAmount !== 0 && $discountAmount !== -0.0) {
            $this->afterpay->create_order_line(
                'DISCOUNT', // Article ID
                'Discount', // Article description
                '1', // Quantity
                (string) $discountAmount, // Unit price
                $this->vatConfig->getAfterpayVATCategory(
                    $this->checkoutSession->getQuote()->getStoreId(),
                    'discount'
                ),
                $discountAmountCompensation
            );
        }
    }

    /**
     * Add shipping fee line
     */
    private function addShippingFeeLine()
    {
        $shipping = $this->checkoutSession->getQuote()->getShippingAddress();
        $shippingFee = $shipping->getShippingInclTax() * 100;
        if ($shipping->getShippingDiscountAmount() > 0.00) {
            $shippingFee = ($shipping->getShippingInclTax() - $shipping->getShippingDiscountAmount()) * 100;
        }
        if ($shippingFee !== 0) {
            $this->afterpay->create_order_line(
                'SHIPPING', // Article ID
                'Shipping', // Article description
                '1', // Quantity
                (string) $shippingFee, // Unit price
                $this->vatConfig->getAfterpayVATCategory(
                    $this->checkoutSession->getQuote()->getStoreId(),
                    'shipping'),
                $shipping->getShippingTaxAmount()
            );
        }
    }

    /**
     * Add payment fee line
     */
    private function addPaymentFeeLine()
    {
        $paymentFee = $this->checkoutSession->getQuote()->getAfterpayPaymentFee() * 100;
        if ($paymentFee !== 0) {
            $this->afterpay->create_order_line(
                'FEE', // Article ID
                'Payment Fee', // Article description
                '1', // Quantity
                (string) $paymentFee, // Unit price
                $this->vatConfig->getAfterpayVATCategory(
                    $this->checkoutSession->getQuote()->getStoreId(),
                    'fee'
                )
            );
        }
    }

    /**
     * Add payment fee line for Fooman Surcharge
     */
    private function addFoomanTotalLines()
    {
        foreach ($this->checkoutSession->getQuote()->getAllAddresses() as $address) {
            $extensionAttributes = $address->getExtensionAttributes();
            if (!$extensionAttributes) {
                continue;
            }
            if (!method_exists($extensionAttributes, 'getFoomanTotalGroup')) {
                return;
            }
            $quoteAddressTotalGroup = $extensionAttributes->getFoomanTotalGroup();
            if (!$quoteAddressTotalGroup) {
                continue;
            }
            $totals = $quoteAddressTotalGroup->getItems();
            if (empty($totals)) {
                return;
            }
            foreach ($totals as $total) {
                $paymentFee = ($total->getBaseAmount() + $total->getBaseTaxAmount()) * 100;
                $this->afterpay->create_order_line(
                    'FEE', // Article ID
                    $total->getLabel(), // Article description
                    '1', // Quantity
                    (string)$paymentFee, // Unit price
                    $this->vatConfig->getAfterpayVATCategory(
                        $this->checkoutSession->getQuote()->getStoreId(),
                        'fee'
                    )
                );
            }
        }
    }

    /**
     * @return bool
     */
    private function isRest(): bool
    {
        return $this->afterpay->useRest;
    }
}
