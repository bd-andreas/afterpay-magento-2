<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Http\Client;

use Afterpay\AfterpayFactory;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;
use Psr\Log\LoggerInterface;

abstract class AbstractTransaction implements ClientInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Logger
     */
    protected $customLogger;

    /**
     * @var AfterpayFactory
     */
    protected $afterpay;

    /**
     * AbstractTransaction constructor.
     *
     * @param LoggerInterface $logger
     * @param Logger $customLogger
     * @param AfterpayFactory $afterpayFactory
     */
    public function __construct(LoggerInterface $logger, Logger $customLogger, AfterpayFactory $afterpayFactory)
    {
        $this->logger = $logger;
        $this->customLogger = $customLogger;
        $this->afterpay = $afterpayFactory->create();

    }

    /**
     * @param TransferInterface $transferObject
     *
     * @return array|mixed
     * @throws ClientException
     */
    public function placeRequest(TransferInterface $transferObject)
    {
        $data = $transferObject->getBody();
        $log = [
            'request' => $data,
            'client' => static::class
        ];
        $response['object'] = [];

        try {
            $response['object'] = $this->execute($data, $transferObject->getClientConfig());
        } catch (\Exception $e) {
            $message = __($e->getMessage() ?: 'Sorry, but something went wrong');
            $this->logger->critical($message);
            throw new ClientException($message);
        } finally {
            $log['response'] = (array) $response['object'];
            $this->customLogger->debug($log);

            // Request log
            $this->debugHelper->debug(
                $data['debug']['payment_method'],
                [
                    'order_request' => $this->afterpay->order
                ],
                true
            );

            // Response log
            $this->debugHelper->debug(
                $data['debug']['payment_method'],
                [
                    'order_result' => $this->afterpay->order_result,
                    'debug' => $this->afterpay->client->getDebugLog()
                ],
                true
            );

        }

        return $response;
    }

    /**
     * @param array $data
     * @param array $config
     *
     * @return \stdClass
     */
    abstract protected function execute(array $data, array $config);

    /**
     * @param array $clientConfig
     *
     * @return bool
     */
    protected function isRestOrder(array $clientConfig): bool
    {
        return isset($clientConfig['has_rest']) && $clientConfig['has_rest'] === true;
    }
}
