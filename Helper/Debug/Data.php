<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Helper\Debug;

use Afterpay\Payment\Logger\Logger;
use Magento\Framework\App\Helper\{AbstractHelper, Context};
use Magento\Framework\{App\Area, DataObject, Escaper, UrlInterface};
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Logger\Monolog;
use Magento\Framework\Session\Storage as Session;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Filesystem\Io\File;

/**
 * AfterPay Debug helper
 */
class Data extends AbstractHelper
{
    /**
     * Security message for masked values
     */
    const SECURITY_MESSAGE = 'REMOVED FOR SECURITY REASONS';

    /**
     * Fields that should be replaced in debug with security message
     *
     * @var array
     */
    private $debugReplacePrivateDataKeys = ['password'];

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * Logging instance
     *
     * @var Logger
     */
    protected $logger;

    /**
     * Log file folder path
     *
     * @var string
     */
    private $folderPath = '/var/log/afterpay/';

    /**
     * Log file folder path
     *
     * @var string
     */
    private $filePath;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Encryption interface
     *
     * @var EncryptorInterface
     */
    protected $encryption;

    /**
     * @var File
     */
    protected $file;

    /**
     * Log file extension
     *
     * @var string
     */
    private $fileExtension = '.txt';

    /**
     * Payment method code
     *
     * @var string
     */
    protected $paymentMethodCode = '';

    /**
     * @var DataObject
     */
    protected $postObject;

    /**
     * @param Context $context
     * @param AttachmentTransportBuilder $attachmentTransportBuilder
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface $storeManager
     * @param Escaper $escaper
     * @param Monolog $logger
     * @param Session $session
     * @param EncryptorInterface $encryption
     * @param File $file
     * @param DataObject $postObject
     *
     * @throws \Exception
     */
    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        Escaper $escaper,
        Logger $logger,
        Session $session,
        EncryptorInterface $encryption,
        File $file,
        DataObject $postObject
    ) {

        parent::__construct($context);
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->storeManager = $storeManager;
        $this->escaper = $escaper;
        $this->logger = $logger;
        $this->session = $session;
        $this->encryption = $encryption;
        $this->file = $file;
        $this->postObject = $postObject;
    }

    /**
     * Debug data
     *
     * @param string $paymentMethod
     * @param $data
     * @param bool $sendmail
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function debug($paymentMethod, $data, $sendmail = false)
    {
        $this->paymentMethodCode = $paymentMethod;
        $this->isDebugEnabled() ? $this->processDebug($data, $sendmail) : false;
    }

    /**
     * Debug enabled
     *
     * @return mixed
     */
    private function isDebugEnabled()
    {
        return $this->scopeConfig->getValue($this->getXmlPathDebugEnabled());
    }

    /**
     * Send email enabled
     *
     * @return mixed
     */
    private function isEmailEnabled()
    {
        return $this->scopeConfig->getValue($this->getXmlPathDebugEmailEnabled());
    }

    /**
     * Debug data using Zend library
     *
     * @param $data
     * @param $sendmail
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function processDebug($data, $sendmail)
    {
        $this->logger->debug(json_encode($data, JSON_PRETTY_PRINT));

        if ($sendmail) {
            $this->sendDebugMail($data);
        }
    }

    /**
     * Filter data for senstive debug info
     *
     * @param $data
     *
     * @return mixed
     */
    private function filterData($data)
    {
        if (\is_array($data)) {
            foreach ($data as $key => $value) {
                if (\in_array($key, $this->getDebugReplacePrivateDataKeys())) {
                    $data[$key] = self::SECURITY_MESSAGE;
                }

                if (\is_object($value)) {
                    $this->filterObject($value);
                } elseif (\is_array($value)) {
                    $this->filterData($value);
                }
            }
        }

        return $data;
    }

    /**
     * Filter debug data object
     *
     * @param $data
     *
     * @return mixed
     */
    private function filterObject($data)
    {
        if (\is_object($data)) {
            foreach ($data as $key => $value) {
                if (\in_array($key, $this->getDebugReplacePrivateDataKeys())) {
                    $data->$key = self::SECURITY_MESSAGE;
                }

                if (\is_object($value)) {
                    $this->filterObject($value);
                } elseif (\is_array($value)) {
                    $this->filterData($value);
                }
            }
        }

        return $data;
    }

    /**
     * Send an email with debug file attached
     *
     * @param $data
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function sendDebugMail($data)
    {
        $recipient = explode(
            ',',
            $this->scopeConfig->getValue($this->getXmlPathDebugRecipientEmail())
        );

        if ($recipient && $this->isEmailEnabled()) {
            // Store base url
            $storeBaseUrl = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_WEB);
            $storeBaseUrl = parse_url($storeBaseUrl); // @codingStandardsIgnoreLine
            $storeBaseUrl = $storeBaseUrl['host'];
            $this->postObject->setData([
                "store_url" => $storeBaseUrl,
                'debug' => json_encode($data, JSON_PRETTY_PRINT)
            ]);

            $transport = $this->transportBuilder
                ->setTemplateIdentifier('debug_email_template')
                ->setTemplateOptions(
                    [
                        'area' => Area::AREA_FRONTEND,
                        'store' => Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars(['data' => $this->postObject])
                ->addTo($recipient)
                ->getTransport();
            try {
                $transport->sendMessage();
            } catch (\Exception $e) {
                $this->debug($this->paymentMethodCode, $e);
            }
        }
    }

    /**
     * Get hashed customer session id
     *
     * @return string
     */
    private function getHashedSessionId(): string
    {
        $visitorSession = $this->session->getVisitorData();
        $visitorSessionId = $visitorSession['session_id'];

        return $this->encryption->hash($visitorSessionId);
    }

    /**
     * Return replace keys for debug data
     *
     * @return array
     */
    private function getDebugReplacePrivateDataKeys(): array
    {
        return $this->debugReplacePrivateDataKeys;
    }

    /**
     * Debug enabled config path
     *
     * @return string
     */
    private function getXmlPathDebugEnabled(): string
    {
        return 'payment/' . $this->paymentMethodCode . '/debug';
    }

    /**
     * Email enabled config path
     *
     * @return string
     */
    private function getXmlPathDebugEmailEnabled(): string
    {
        return 'payment/' . $this->paymentMethodCode . '/debug_email';
    }

    /**
     * Recipient email config path
     *
     * @return string
     */
    protected function getXmlPathDebugRecipientEmail(): string
    {
        return 'payment/' . $this->paymentMethodCode . '/debug_email_address';
    }
}
