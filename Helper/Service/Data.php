<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Helper\Service;

use Magento\Config\Model\Config\Backend\Encrypted;
use Magento\Framework\App\Helper\{AbstractHelper, Context};
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * AfterPay service helper
 */
class Data extends AbstractHelper
{
    /**
     * Invoice not capture
     */
    const INVOICE_NOT_CAPTURE_ACTIVE = 1;

    /**
     * Order statuses
     */
    const ORDER_STATUS_PENDING = 'pending';
    const ORDER_STATUS_PROCESSING = 'processing';

    /**
     * Test mode label
     */
    const TEST_MODE_LABEL = 'Test mode, orders will not be fulfilled.';

    /**
     * XML Fields for configuration constants
     */
    const XML_FIELD_TEST_MODE_ACTIVE = 'testmode';
    const XML_FIELD_TEST_MODE_MERCHANT_ID = 'testmode_merchant_id';
    const XML_FIELD_TEST_MODE_PORTFOLIO_ID = 'testmode_portfolio_id';
    const XML_FIELD_TEST_MODE_PASSWORD = 'testmode_password';
    const XML_FIELD_TEST_MODE_PUSH_PASSWORD = 'testmode_push_password';
    const XML_FIELD_PROD_MODE_MERCHANT_ID = 'production_merchant_id';
    const XML_FIELD_PROD_MODE_PORTFOLIO_ID = 'production_portfolio_id';
    const XML_FIELD_PROD_MODE_API_KEY = 'production_api_key';
    const XML_FIELD_TEST_MODE_API_KEY = 'testmode_api_key';
    const XML_FIELD_PROD_MODE_PASSWORD = 'production_password';
    const XML_FIELD_PROD_MODE_PUSH_PASSWORD = 'production_push_password';
    const XML_FIELD_REFUND_ENABLED = 'payment/afterpay_refund/active';
    const XML_FIELD_HAS_REST = 'has_rest';

    /**
     * Afterpay SVG icon URL
     */
    const AFTERPAY_SVG_ICON = 'https://cdn.myafterpay.com/logo/AfterPay_logo.svg';

    // All payment method const names

    // NL
    const AFTERPAY_NL_DI = 'afterpay_nl_digital_invoice';
    const AFTERPAY_NL_DI_EXTRA = 'afterpay_nl_digital_invoice_extra';
    const AFTERPAY_NL_DD = 'afterpay_nl_direct_debit';
    const AFTERPAY_NL_B2B = 'afterpay_nl_business_2_business';

    // DE
    const AFTERPAY_DE_DI = 'afterpay_de_invoice';
    const AFTERPAY_DE_DI_EXTRA = 'afterpay_de_invoice_extra';
    const AFTERPAY_DE_DD = 'afterpay_de_direct_debit';
    const AFTERPAY_DE_IN = 'afterpay_de_installment';

    // BE
    const AFTERPAY_BE_DI = 'afterpay_be_digital_invoice';
    const AFTERPAY_BE_DI_EXTRA = 'afterpay_be_digital_invoice_extra';

    // AT
    const AFTERPAY_AT_OI = 'afterpay_at_open_invoice';
    const AFTERPAY_AT_DD = 'afterpay_at_direct_debit';
    const AFTERPAY_AT_IN = 'afterpay_at_installment';

    // CH
    const AFTERPAY_CH_OI = 'afterpay_ch_open_invoice';

    // SE
    const AFTERPAY_SE_OI = 'afterpay_se_open_invoice';
    const AFTERPAY_SE_IN = 'afterpay_se_installment';

    // FI
    const AFTERPAY_FI_OI = 'afterpay_fi_open_invoice';
    const AFTERPAY_FI_IN = 'afterpay_fi_installment';

    // NO
    const AFTERPAY_NO_OI = 'afterpay_no_open_invoice';
    const AFTERPAY_NO_IN = 'afterpay_no_installment';
    const AFTERPAY_NO_FX = 'afterpay_no_flex';

    // DK
    const AFTERPAY_DK_DI = 'afterpay_dk_digital_invoice';

    /**
     * Iso languages for payment methods types
     *
     * @var $isoLanguages
     */
    protected $isoLanguages = [
        'afterpay_nl_digital_invoice' => 'NL',
        'afterpay_nl_digital_invoice_extra' => 'NL',
        'afterpay_nl_direct_debit' => 'NL',
        'afterpay_nl_business_2_business' => 'NL',
        'afterpay_be_digital_invoice' => 'NL',
        'afterpay_be_digital_invoice_extra' => 'NL',
        'afterpay_de_invoice' => 'DE',
        'afterpay_de_invoice_extra' => 'DE',
        'afterpay_de_direct_debit' => 'DE',
        'afterpay_at_open_invoice' => 'DE',
        'afterpay_at_direct_debit' => 'DE',
        'afterpay_at_installment' => 'DE',
        'afterpay_de_installment' => 'DE',
        'afterpay_ch_open_invoice' => 'DE',
        'afterpay_se_open_invoice' => 'SE',
        'afterpay_se_installment' => 'SE',
        'afterpay_fi_open_invoice' => 'FI',
        'afterpay_fi_installment' => 'FI',
        'afterpay_no_open_invoice' => 'NO',
        'afterpay_no_installment' => 'NO',
        'afterpay_no_flex' => 'NO',
        'afterpay_dk_digital_invoice' => 'DK'
    ];

    /**
     * Format date
     *
     * @var DateTime
     */
    protected $dateTime;

    /**
     * Decryptor
     *
     * @var Encrypted
     */
    protected $encrypted;

    /**
     * @param Context $context
     * @param DateTime $dateTime
     * @param Encrypted $encrypted
     */
    public function __construct(
        Context $context,
        DateTime $dateTime,
        Encrypted $encrypted
    ) {
        $this->dateTime = $dateTime;
        $this->encrypted = $encrypted;

        parent::__construct($context);
    }

    /**
     * Get customer initials
     *
     * @param string $firstName first name
     *
     * @return string
     */
    public function getInitials($firstName): string
    {
        $nameParts = explode(' ', trim($firstName));
        $initials = '';

        foreach ($nameParts as $namePart) {
            $initials .= ucfirst($namePart[0]) . '.';
        }

        return $initials;
    }

    /**
     * Format date of birth for request
     *
     * @param string $dob date of birth
     *
     * @return string|null
     * @throws \Exception
     */
    public function formatDob($dob)
    {
        if ($dob) {
            return (string) str_replace(
                ' ',
                'T',
                $this->dateTime->formatDate(new \DateTime($dob), true)
            );
        }

        return null;
    }

    /**
     * Get ISO language depending on payment method
     *
     * @param string $paymentMethod payment method code
     *
     * @return string
     */
    public function getIsoLanguage($paymentMethod): string
    {
        return $this->isoLanguages[$paymentMethod];
    }

    /**
     * Returns decrypted push notification password for provided payment method
     *
     * @param $paymentMethodCode
     *
     * @return string
     */
    public function getPushPassword($paymentMethodCode)
    {
        return $this->encrypted->processValue(
            $this->scopeConfig->getValue('payment/' . $paymentMethodCode . '/' . self::XML_FIELD_TEST_MODE_ACTIVE) ?
                $this->scopeConfig->getValue(
                    'payment/' . $paymentMethodCode . '/' . self::XML_FIELD_TEST_MODE_PUSH_PASSWORD
                ) :
                $this->scopeConfig->getValue(
                    'payment/' . $paymentMethodCode . '/' . self::XML_FIELD_PROD_MODE_PUSH_PASSWORD
                )
        );
    }

    /**
     * @param $data
     * @param null $key
     *
     * @return null|string
     */
    public function readAdditionalInfo($data, $key = null)
    {
        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return null;
    }

    /**
     * @param OrderInterface $order
     *
     * @return bool
     */
    public function isAfterpayOrder($order)
    {
        $payment = $order->getPayment();
        if ($payment) {
            return strpos($payment->getMethod(), 'afterpay') === 0;
        }

        return false;
    }

    /**
     * @param $config_path
     * @param null $scopeCode
     *
     * @return mixed
     */
    public function getStoreConfig($config_path, $scopeCode = null)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * Split address
     *
     * @param $address
     *
     * @return array
     * @throws LocalizedException
     */
    public function getSplitStreet($address)
    {
        $address = \is_array($address) ? implode($address, ' ') : $address;
        $ret = [
            'streetname' => '',
            'housenumber' => '',
            'houseNumberAddition' => '',
        ];

        if (preg_match('/^(.*?)([0-9]+)(.*)/s', $address, $matches)) {
            if ('' === $matches[1]) {
                // Number at beginning
                $ret['housenumber'] = trim($matches[2]);
                $ret['streetname'] = trim($matches[3]);
            } else {
                // Number at end
                $ret['streetname'] = trim($matches[1]);
                $ret['housenumber'] = trim($matches[2]);
                $ret['houseNumberAddition'] = trim($matches[3]);
            }
        } else {
            $message = 'The house number addition of the shipping address is missing. Please check your shipping details or contact our customer service.';
            throw new LocalizedException(__($message));
        }

        return $ret;
    }

    /**
     * @param int $connectionType
     * @param bool $alt
     *
     * @return string
     */
    public function getConnectionType(int $connectionType, bool $alt = false): string
    {
        $connectionTypeMapped = ['production', 'testmode', 'sandbox'];
        if ($alt) {
            $connectionTypeMapped = ['live', 'test', 'sandbox'];
        }
        return $connectionTypeMapped[$connectionType];
    }

    /**
     * @param $paymentMethod string
     *
     * @param null $scopeCode
     *
     * @return mixed
     */
    public function getConfiguration($paymentMethod, $scopeCode = null)
    {
        $connectionType = $this->getStoreConfig(
            sprintf('payment/%s/testmode', $paymentMethod),
            $scopeCode
        );
        $result['modus'] = $this->getConnectionType($connectionType);
        $result['mode'] = $this->getConnectionType($connectionType, true);
        $result['apiKey'] = $this->getStoreConfig(
            sprintf('payment/%s/%s_api_key', $paymentMethod, $result['modus']),
            $scopeCode
        );
        return $result;
    }
}
