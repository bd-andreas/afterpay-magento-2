<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var WriterInterface
     */
    protected $writer;

    /**
     * EAV setup factory
     *
     * @var EavSetup
     */
    private $eavSetup;

    /**
     * Init
     *
     * @param EavSetup $eavSetup
     * @param WriterInterface $writer
     */
    public function __construct(EavSetup $eavSetup, WriterInterface $writer)
    {
        $this->eavSetup = $eavSetup;
        $this->writer = $writer;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) // @codingStandardsIgnoreLine
    {
        $setup->startSetup();

        $this->eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'cocnumber',
            [
                'type' => 'static',
                'input' => 'text',
                'label' => 'CoC number',
                'required' => false,
                'system' => 0,
            ]
        );

        $defaultConfigValues = [
            'payment/afterpay_capture/active' => '3',
            'payment/afterpay_refund/active' => '1'
        ];
        foreach ($defaultConfigValues as $path => $value) {
            $this->writer->save($path, $value, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        }
        $setup->endSetup();
    }
}
