<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Plugin;

use Afterpay\Payment\Model\Config\Advanced;
use Magento\Payment\Model\Method\Adapter;

class OrderPaymentPlugin
{
    /**
     * @var Advanced
     */
    private $advancedConfig;

    /**
     * @param Advanced $advancedConfig
     */
    public function __construct(Advanced $advancedConfig)
    {
        $this->advancedConfig = $advancedConfig;
    }

    /**
     * Dynamically decide which payment action to return for Afterpay methods, using 'Enable Capture' setting
     *
     * @param Adapter $subject
     * @param callable $proceed
     * @return string
     */
    public function aroundGetConfigPaymentAction(Adapter $subject, callable $proceed)
    {
        $result = $proceed();
        if ($this->isAfterpayMethod($subject->getCode())) {
            return $this->advancedConfig->captureModeAuto() ? 'authorize_capture' : 'authorize';
        }

        return $result;
    }

    /**
     * @param string $code
     * @return bool
     */
    private function isAfterpayMethod($code)
    {
        return strpos($code, 'afterpay') === 0;
    }
}
