<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Plugin;

use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Model\Config\Advanced;
use Afterpay\Payment\Model\Order\Processor;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\ShipmentInterface;
use Magento\Sales\Model\Order\Shipment;

class ShipmentPlugin
{
    /**
     * @var Advanced
     */
    private $advancedConfig;

    /**
     * @var Processor
     */
    private $orderProcessor;

    /**
     * @var Data
     */
    private $dataHelper;

    /**
     * @param Advanced $advancedConfig
     * @param Processor $orderProcessor
     * @param Data $dataHelper
     */
    public function __construct(
        Advanced $advancedConfig,
        Processor $orderProcessor,
        Data $dataHelper
    ) {
        $this->advancedConfig = $advancedConfig;
        $this->orderProcessor = $orderProcessor;
        $this->dataHelper = $dataHelper;
    }

    /**
     * Before shipment registration try and invoice the order if config has been set
     *
     * @param ShipmentInterface|Shipment $subject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeRegister(ShipmentInterface $subject)
    {
        $order = $subject->getOrder();

        if ($this->invoiceOnShipmentAvailable($order)) {
            try {
                $this->orderProcessor->capture($order);
                $order->addStatusHistoryComment(
                    __('Invoice created automatically on shipment')
                );
            } catch (LocalizedException $e) {
                $order->addStatusHistoryComment(__('Auto invoice on shipment failed: %1', $e->getMessage()));
            }
        }
    }

    /**
     * @param OrderInterface $order
     * @return bool
     */
    private function invoiceOnShipmentAvailable(OrderInterface $order)
    {
        return $this->dataHelper->isAfterpayOrder($order)
            && $this->advancedConfig->captureModeShipping($order->getStoreId());
    }
}
