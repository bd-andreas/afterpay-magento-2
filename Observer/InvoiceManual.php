<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Observer;

use Afterpay\Payment\Model\Config\Advanced;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DB\Transaction;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Service\InvoiceService;

class InvoiceManual implements ObserverInterface
{
    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Transaction
     */
    protected $transaction;

    /**
     * @var Advanced
     */
    private $advancedConfig;

    /**
     * InvoiceOffline constructor.
     *
     * @param InvoiceService $invoiceService
     * @param ScopeConfigInterface $scopeConfig
     * @param Transaction $transaction
     * @param Advanced $advancedConfig
     */
    public function __construct(
        InvoiceService $invoiceService,
        ScopeConfigInterface $scopeConfig,
        Transaction $transaction,
        Advanced $advancedConfig
    ) {
        $this->invoiceService = $invoiceService;
        $this->scopeConfig = $scopeConfig;
        $this->transaction = $transaction;
        $this->advancedConfig = $advancedConfig;
    }

    /**
     * @param Observer $observer
     *
     * @throws LocalizedException
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        $order = $event->getDataByKey('order');
        if ($this->advancedConfig->captureModeManual($order->getStoreId()) &&
            $this->advancedConfig->invoiceNotCaptureActive($order->getStoreId())) {
            /** @var $payment \Magento\Sales\Model\Order\Payment\Interceptor */
            $payment = $order->getPayment();
            /** @var $paymentInstance \Magento\Payment\Model\Method\Adapter\Interceptor */
            $paymentInstance = $payment->getMethodInstance();
            if ($this->isAfterpayMethod($paymentInstance->getCode())) {
                $invoice = $this->invoiceService->prepareInvoice($order);
                $invoice->setRequestedCaptureCase(Invoice::NOT_CAPTURE);
                $invoice->register();
                $this->transaction
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
                $this->transaction->save();
            }
        }
    }

    /**
     * @param $code
     *
     * @return bool
     */
    protected function isAfterpayMethod($code)
    {
        return strpos($code, 'afterpay') === 0;
    }
}
