<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Observer;

use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Model\Config\Advanced;
use Afterpay\Payment\Model\Order\Processor;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;

class CaptureOnStatusChange implements ObserverInterface
{
    /**
     * @var Advanced
     */
    private $advancedConfig;

    /**
     * @var Processor
     */
    private $orderProcessor;

    /**
     * @var Data
     */
    private $dataHelper;

    /**
     * @param Advanced $advancedConfig
     * @param Processor $orderProcessor
     * @param Data $dataHelper
     */
    public function __construct(
        Advanced $advancedConfig,
        Processor $orderProcessor,
        Data $dataHelper
    ) {
        $this->advancedConfig = $advancedConfig;
        $this->orderProcessor = $orderProcessor;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getData('order');

        if ($this->dataHelper->isAfterpayOrder($order)
            && $this->allowCapture($order->getStatus(), $order->getStoreId())
        ) {
            try {
                $this->orderProcessor->capture($order);
                $order->addStatusHistoryComment(
                    __(
                        'Automatically captured payment on status %1',
                        $this->advancedConfig->autoCaptureOrderStatus($order->getStoreId())
                    )
                );
            } catch (LocalizedException $exception) {
                $order->addStatusHistoryComment(
                    __(
                        'Failed to automatically capture payment on status %1. Error: %2',
                        $this->advancedConfig->autoCaptureOrderStatus($order->getStoreId()),
                        $exception->getMessage()
                    )
                );
            }
        }
    }

    /**
     * @param $status
     * @return bool
     */
    private function allowCapture($status, $storeId)
    {
        return $this->advancedConfig->captureModeStatus($storeId)
            && $status === $this->advancedConfig->autoCaptureOrderStatus($storeId);
    }
}
