<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */
namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Helper\ImageFactory;
use Magento\Catalog\Helper\Image;

/**
 * Class AddProductImageToQuoteItem
 *
 * @package Afterpay\Payment\Observer
 */
class AddProductImageToQuoteItem implements ObserverInterface
{
    /**
     * @var ImageFactory
     */
    private $helperFactory;

    /**
     * AddProductImageToQuoteItem constructor.
     *
     * @param ImageFactory $helperFactory
     */
    public function __construct(
        ImageFactory $helperFactory
    )
    {
        $this->helperFactory = $helperFactory;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        $product = $event->getDataByKey('product');
        $quoteItem = $event->getDataByKey('quote_item');
        $imageUrl = $this->getImage($product)->getUrl();
        $quoteItem->setAfterpayProductImage($imageUrl);
    }

    /**
     * @param $product
     *
     * @return Image
     */
    private function getImage($product): Image
    {
        return $this->helperFactory->create()->init($product, 'cart_page_product_thumbnail');
    }
}
