<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Block\Adminhtml\Sales\Order;

use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Template;
use Magento\Payment\Model\MethodInterface;
use Magento\Sales\Model\Order;

/**
 * AfterPay service fee block for sales order
 */
class Fee extends Template
{
    /**
     * Add payment fee to totals
     *
     * @return \Afterpay\Payment\Block\Adminhtml\Sales\Order\Fee
     */
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        /* @var Order $order */
        $order = $parent->getOrder();
        /* @var MethodInterface $method */
        $method = $order->getPayment()->getMethodInstance();

        if ($order->getData('afterpay_payment_fee') > 0) {
            $this->addPaymentFee(
                $method->getCode() . '_fee',
                $order->getData('afterpay_payment_fee'),
                $method->getConfigData('fee_title')
            );
        }

        return $this;
    }

    /**
     * Add payment fee string
     *
     * @param string $code
     * @param string $value
     * @param string $title
     */
    private function addPaymentFee($code, $value, $title)
    {
        $fee = new DataObject(
            [
                'code' => $code,
                'value' => $value,
                'label' => $title,
            ]
        );

        $this->getParentBlock()->addTotal($fee, 'shipping');
    }
}
