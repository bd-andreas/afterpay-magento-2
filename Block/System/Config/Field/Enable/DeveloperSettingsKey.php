<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

namespace Afterpay\Payment\Block\System\Config\Field\Enable;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\Url;
use Magento\Directory\Helper\Data;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Helper\Js;

/**
 * AfterPay adv. configuration enable
 */
class DeveloperSettingsKey extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var Js
     */
    protected $jsHelper;

    /**
     * @var array
     */
    protected $fieldConfig;

    /**
     * @var Url
     */
    protected $url;

    /**
     * @var Data
     */
    protected $directoryHelper;

    /**
     * @param Context $context
     * @param Url $url
     * @param Js $jsHelper
     * @param Data $directoryHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Url $url,
        Js $jsHelper,
        Data $directoryHelper,
        array $data = []
    ) {
    
        parent::__construct($context, $data);
        $this->url = $url;
        $this->jsHelper = $jsHelper;
        $this->directoryHelper = $directoryHelper;
    }

    /**
     * Return script and button for adv. configuration enabling
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $this->fieldConfig = explode('/', $element->getFieldConfig()['path']);
        /** @var  $html */
        $html = '<button id="afterpay_advanced_configuration_be" class="button afterpay_configure close" type="button">
                <span class="state-closed">Enable Advanced Settings</span>
                </button>';

        $args = [
            $this->mergeConfigArea('_afterpay_developer_settings_md5k'),
            $this->mergeConfigArea('_afterpay_developer_settings_advanced_developer_settings'),
            $this->mergeConfigArea('_afterpay_developer_settings_md5k'),
            $this->mergeConfigArea('_afterpay_developer_settings_enable_afterpay_configuration')
        ];

        $jsString = '
            var md5_fieldbe = jQuery("#%s"),
                afterpay_confbe = jQuery("#row_%s"),
                cookieNamebe = "advancedConfigBE",
                inputRowsbe = jQuery("#row_%s, #row_%s");
            md5_fieldbe.val("");
            if (jQuery.cookie(cookieNamebe) == 1) {
                inputRowsbe.hide();
            } else {
                afterpay_confbe.css("display","none");
            }
            jQuery("#afterpay_advanced_configuration_be").click(function () {
                md5_fieldbe.removeClass("required-entry _required mage-error");
                if ("hc35aDjCDu7esWE" == md5_fieldbe.val()) {
                    afterpay_confbe.css("display","");
                    jQuery.cookie(cookieNamebe, 1);
                    inputRowsbe.hide();
                } else {
                    md5_fieldbe.addClass("required-entry _required mage-error");
                }
            });';

        $jsString = vsprintf($jsString, $args);
        return $html . $this->jsHelper->getScript(
            'require([\'jquery\', \'jquery/jquery.cookie\'], function(jQuery){
            jQuery(document).ready( function() {' . $jsString . '}); 
            });'
        );
    }

    /**
     * Return field ID merged together with config area, which is payment_*
     * where '*' is among 'us', 'other', 'aus' and many other country codes
     *
     * @param $field
     *
     * @return string
     */
    protected function mergeConfigArea($field)
    {
        return $this->fieldConfig[0] . $field;
    }
}
