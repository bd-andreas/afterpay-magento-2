<?php

namespace Afterpay\Payment\Test\Unit\Model\Config;

use Afterpay\Payment\Model\Config\Visitor;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

class VisitorTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    protected $objectHelper;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $mockScopeConfig;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $mockRemoteAddress;

    protected function setUp()
    {
        parent::setUp();
        $this->objectHelper = new ObjectManager($this);
        $this->mockScopeConfig = $this->getMockBuilder(ScopeConfigInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockRemoteAddress = $this->getMockBuilder(\Magento\Framework\HTTP\PhpEnvironment\RemoteAddress::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testAllowsWhitelistedIps()
    {
        $subject = $this->objectHelper->getObject(
            Visitor::class,
            [
                'scopConfig' => $this->mockScopeConfig,
                'remoteAddress' => $this->mockRemoteAddress
            ]
        );
        $this->mockScopeConfig->method('getValue')->willReturn('valid.ip.address');
        $this->mockRemoteAddress->method('getRemoteAddress')->willReturn('valid.ip.address');

        $this->assertEquals(true, $subject->allowedByIp());
    }

    public function testRestrictsWhenNecessary()
    {
        $subject = $this->objectHelper->getObject(
            Visitor::class,
            [
                'scopeConfig' => $this->mockScopeConfig,
                'remoteAddress' => $this->mockRemoteAddress
            ]
        );

        $this->mockScopeConfig->method('getValue')->willReturn('valid.ip.address');
        $this->mockRemoteAddress->method('getRemoteAddress')->willReturn('invalid.ip');

        $this->assertEquals(false, $subject->allowedByIp());
    }

    public function testAllowsWhenValueEmpty()
    {
        $subject = $this->objectHelper->getObject(
            Visitor::class,
            [
                'scopConfig' => $this->mockScopeConfig,
                'remoteAddress' => $this->mockRemoteAddress
            ]
        );
        $this->mockScopeConfig->method('getValue')->willReturn(null);

        $this->assertEquals(true, $subject->allowedByIp());
    }
}
